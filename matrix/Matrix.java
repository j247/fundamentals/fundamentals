package matrix;

public class Matrix 
{
	public static void main(String[] args) 
	{
		double[][] studentNotes = new double[3][4]; // 3X4 rows X columns

		studentNotes[0][0] = 10;
		studentNotes[0][1] = 7;
		studentNotes[0][2] = 9;
		studentNotes[0][3] = 9.5;
		
		studentNotes[1][0] = 9;
		studentNotes[1][1] = 8;
		studentNotes[1][2] = 7;
		studentNotes[1][3] = 9;
		
		studentNotes[2][0] = 8;
		studentNotes[2][1] = 9;
		studentNotes[2][2] = 10;
		studentNotes[2][3] = 7;
		
		for (int i=0; i<studentNotes.length; i++)
		{
			//System.out.print(studentNotes[i] + " ");
			for (int j=0; j<studentNotes[i].length; j++)
			{
				System.out.print(studentNotes[i][j] + " - ");
			}
			System.out.println();
		}
	}
}
