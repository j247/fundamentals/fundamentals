package matrix;

public class ThreeDimmensional 
{

	public static void main(String[] args) 
	{
		int[][][] matrixThree = new int[3][3][3];// i j k

		for (int i=0; i<matrixThree.length; i++)
		{
			for (int j=0; j<matrixThree[i].length; j++)
			{
				for (int k=0; k<matrixThree[i][j].length; k++)
				{
					System.out.println("i = " + i + " - j = " + j + " - k = " + k);
					matrixThree[i][j][k] = i + j + k;
				}
			}
		}
	}

}
