package operators;

public class Arithmetic {

	public static void main(String[] args) {
		
		int result = 1 + 2;
		System.out.println("1+2: " + result);
		
		result = result - 1;
		System.out.println("result -1: " +result);
		
		result = result * 2;
		System.out.println("result * 2: " +result);
		
		result = result / 2;
		System.out.println("result / 2: " +result);
		
		result = result + 8;
		System.out.println("result + 8: " +result);
		
		result = result % 7;
		System.out.println("result % 7: " +result);
		
			
		result = result + 1;
		System.out.println("result + 1: " +result);
		
		result++;
		System.out.println("result++ " +result);
		
					
		result--;
		System.out.println("result-- " +result);
		
	}

}
