package control_structures;

public class IfELse 
{
	public static void main(String[] args) 
	{
		int age = 18;
		//combine operators (Relational)
		if (age >= 18) 
		{
			System.out.println("Mayor de edad en Colombia");
		}
		else
		{
			System.out.println("Menor de edad en Colombia");
		}
		
		//combine operators (Logics)
		int value1 = 2;
		int value2 = 5;
		
		if (value1 < 5 && value2 > 5)
		{
			System.out.println("AND && --> Verdadero");
		}
		else
		{
			System.out.println("AND && --> Falso");
		}
		
		if (value1 < 5 || value2 > 5)
		{
			System.out.println("OR || --> Verdadero");
		}
		else
		{
			System.out.println("OR || --> Falso");
		}
	}
}
