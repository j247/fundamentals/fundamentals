package control_structures;

public class SwitchCase 
{
	public static void main(String[] args) 
	{
		
		int day = 3;
		
		if (day == 1){
			System.out.println("Domingo");
		} else if (day == 2){
			System.out.println("Lunes");
		} else if (day == 3){
			System.out.println("Martes");
		} else if (day == 4){
			System.out.println("Miercoles");
		} else if (day == 5){
			System.out.println("Jueves");
		} else if (day == 6){
			System.out.println("Viernes");
		} else if (day == 7){
			System.out.println("S�bado");
		} else {
			System.out.println("No es un d�a v�lido");
		}
		
//		Switch Case
		switch(day){
		case 1: System.out.println("Domingo"); break;
		case 2: System.out.println("Lunes"); break;
		case 3: System.out.println("Martes"); break;
		case 4: System.out.println("Miercoles"); break;
		case 5: System.out.println("Jueves"); break;
		case 6: System.out.println("Viernes"); break;
		case 7: System.out.println("S�bado"); break;
		default: System.out.println("No es un d�a v�lido");
		}
		
		
	}
}

