package control_structures;

public class While 
{
	public static void main(String[] args) 
	{
		int i = 1; //count 
		int max = 10;
		
		System.out.println("Contar hasta: " + max);
		
		while (i < max) 
		{
			System.out.println("Valor de variable i: " + i);
			i++; // i = i + 1; 
		}		
	}
}
