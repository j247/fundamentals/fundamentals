package control_structures;

public class DoWhile 
{
	public static void main(String[] args) 
	{
		int i = 1;
		int max = 12;
		
		do {
			i++;
			System.out.println("Valor de variable i: " + i);
		} while (i < max);		
		
	}
}
