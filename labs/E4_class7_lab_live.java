package labs;

import java.util.Scanner;

/*CONSIDERE UN SALON DE CLASE, IMPLEMENTE UN ALGORITMO 
 * QUE PERMITA HALLAR EL PESO PROMEDIO (KG)
 *  DE LOS ESTUDIANTES DEL AULA*/
public class E4_class7_lab_live 
{
	static Scanner scan = new Scanner(System.in);
	static double promedio = 0;
	static double sumaTotal = 0;
	static int cantidadAlumnos = 0;
				
		public static void main(String[] args)
		{			
			System.out.println("Ingrese cantidad alumnos");
			cantidadAlumnos = scan.nextInt();
			double peso[] = new double[cantidadAlumnos];
			
			for (int i = 0; i < peso.length; i++) 
			{
				System.out.println("Ingresar peso de persona "+i);
				peso[i] = scan.nextDouble();
				sumaTotal = sumaTotal + peso[i];
			}
			
			promedio = sumaTotal/ cantidadAlumnos;
			
			System.out.println("El peso en KG total de estudiantes es: " + sumaTotal);
			System.out.println("El promedio de peso en KG de los estudiantes es: " + promedio);
		}
}
