package labs;

import java.util.Scanner;
/* Permitir que se ingrese un n�mero entero por teclado,
 *  una vez hecho esto, validar que el digito ingresado,
 *  cumpla con la condici�n, valores entre 345 y 500.
 *  El ingreso de n�mero, no debe parar o terminar, hasta que se ingrese un valor valido.
 * */

public class E1_insert_number_validate // inserte n�mero, validaci�n 
{
	public static void main(String[] args)
	{
        
        Scanner scan = new Scanner(System.in);
        
        boolean validar = false;
        
        do 
        {
        	System.out.println("Ingrese n�meros validos, valores entre 345 y 500");
        	        
            int numero = scan.nextInt();

            if (numero >=345 && numero <= 500)
            {
                validar = true;                
                System.out.println("N�mero OK: " + numero);
            } 
            else 
            {
               System.out.println("inv�lida, digite nuevamente.");
            }
            
        } while (!validar);
        
    }
}
