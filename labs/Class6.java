package labs;

import java.util.Scanner;

public class Class6 
{
	public static void main(String[] args)
	{
		Scanner scan = new Scanner(System.in);
	
		System.out.println("Hola Que edad tienes? \n"  
				+ "Escribe y presiona ENTER al terminar");
		int age = scan.nextInt();
		
		System.out.println("Cu�l es su nombre? \n"
				+ "Escribe y presiona ENTER al terminar");
		String name = scan.next();

		System.out.println("Genial, " + name + " que buena edad: " + age  );
		scan.close();
		
	}
}
