package variables;

public class Variables {

	public static void main(String[] args) 
	{
		//Numeric
		int age = 20; 
		double height = 1.81;
		long age_long = 200;
		
		//Text
		String name = "Mark Acts";
		String nameMascot = "Luke";
		String year = "2021";
		
		//Boolean
		boolean indicator = true; 
		
		System.out.println("Edad: " + age);
		System.out.println("Nombre: " + name);
		
		System.out.println("Verdadero o Falso: " + indicator);		
		
	}

}
